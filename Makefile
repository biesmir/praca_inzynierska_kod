arm: lib
	$(MAKE) -C stm32f4-discovery

lib:
	$(MAKE) -C praca_inzynierska_lib

run:
	renode -e ' s @arm.resc'

clean:
	$(MAKE) -C praca_inzynierska_lib clean
	$(MAKE) -C stm32f4-discovery clean

