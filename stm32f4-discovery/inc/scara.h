#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "matrix.h"

void dh_scara(const float q1, const float q2, const float q3, const float q4, const float a1,
	      const float a2, const float d1, float* K);


void exp_scara_basic(const float q1, const float q2, const float q3, const float q4, const float a1,
		     const float a2, const float d1, float* K);
