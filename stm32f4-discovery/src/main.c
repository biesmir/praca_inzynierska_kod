/**
  ******************************************************************************
  * @file    IO_Toggle/main.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    19-September-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f4_discovery.h"
#include "stm32f4xx_conf.h" // again, added because ST didn't put it here ?

#include "../../praca_inzynierska_lib/inc/mprint.h"
#include "../../praca_inzynierska_lib/inc/double_pendulum.h"
#include "scara.h"
#include <stdio.h>
#include <math.h>
/** @addtogroup STM32F4_Discovery_Peripheral_Examples
  * @{
  */

int _write(int fd, const char *ptr, int nbytes) {
  const char *buf = ptr;
  for(int i=0; i<nbytes;i++) {
    USART_SendData(USART2, buf[i]);

  }
  return nbytes;
}
/** @addtogroup IO_Toggle
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
GPIO_InitTypeDef  GPIO_InitStructure;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void Delay(__IO uint32_t nCount);
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f4xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
        system_stm32f4xx.c file
     */
/* FPU initialization */
SCB->CPACR |= ((3 << 10*2)|(3 << 11*2));
 
CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
/* DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; /\*  *\/ */
 volatile unsigned int *DWT_CYCCNT   = (volatile unsigned int *)0xE0001004; //address of the register

 
volatile unsigned int *DWT_CONTROL  = (volatile unsigned int *)0xE0001000; //address of the register

 
volatile unsigned int *DWT_LAR      = (volatile unsigned int *)0xE0001FB0; //address of the register

 
volatile unsigned int *SCB_DEMCR    = (volatile unsigned int *)0xE000EDFC; //address of the register


  *DWT_LAR = 0xC5ACCE55; // unlock (CM7)

 
  *SCB_DEMCR |= 0x01000000;

 
  *DWT_CYCCNT = 1; // reset the counter

 
  //  *DWT_CONTROL |= 1 ; // enable the counter
 
RCC_APB2PeriphClockCmd(RCC_APB2Periph_SDIO, ENABLE);
RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

 GPIO_InitTypeDef gpio;

GPIO_StructInit(&gpio);
gpio.GPIO_Pin = GPIO_Pin_2;
gpio.GPIO_Mode = GPIO_Mode_AF;
GPIO_Init(GPIOA, &gpio);
 
gpio.GPIO_Pin = GPIO_Pin_3;
gpio.GPIO_Mode = GPIO_Mode_AIN;
GPIO_Init(GPIOA, &gpio);
	
USART_InitTypeDef uart;
 
USART_StructInit(&uart);
uart.USART_BaudRate = 115200;
USART_Init(USART2, &uart);

USART_Cmd(USART2, ENABLE);
  /* GPIOD Periph clock enable */
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 /* printf("timer:%d\n", *DWT_CYCCNT); /\*  *\/ */
 float tmp = 1.2333;
 int tmp_int = 100;
 float q1;
 float q2;
 float matrix1[16] = {1,2,3,4,5,6,7,8,9};//{1, 0, 0, 0, 1, 0, 0, 0, 1};
 float matrix2[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
 float matrix3[16];
 float matrix4[16];
 double matrix5[16];
 float matrix6[16] = {   0.299745343277013,   0.954019249902090,                   0,   1.960279050830758,
			 -0.954019249902090,   0.299745343277013,                   0,   0.765583580380127,
			 0,                   0,   1.000000000000000,                   0,
			 0,                   0,                   0,   1.000000000000000};
      printf("init %d\n", sizeof(matrix1));
      Delay(5000000);      
      dh_edda(1.1, 10.2, 3.0, 2.0, matrix3);
      
      exp_edda_basic(1.1, 10.2, (float)3.0, 2.0, matrix4);
      
      printf("dh\n");
      mprint(matrix3, 4, 4);
      edda_inv(&q1, &q2, (float)3.0, (float)2.0, matrix3);
      printf("q1=%.10f q2=%.10f\n", q1, q2);
      
      edda_inv(&q1, &q2, (float)3.0, (float)2.0, matrix6);
      printf("q1=%.10f q2=%.10f\n", q1, q2);      

      /* printf("exp\n"); */
      /* mprint(matrix4, 4, 4); */
      /* printf("\n"); */

      /* dh_edda_double(1.1, 10.2, 3.0, 2.0, matrix5); */
      /* for(int i=0;i<16;i++) */
      /* 	printf("%f\n", matrix5[i]); */

      /* exp_scara_basic((float)1.1, (float)10.2, (float)2.0, (float)1.0, (float)1.0, (float)2.0, (float)4, matrix3); */
      
      /* dh_scara((float)1.1, (float)10.2, (float)2.0, (float)1.0, (float)1.0, (float)2.0, (float)4, matrix4); */

      /* printf("exp scara\n"); */
      /* mprint(matrix3, 4, 4); */
      /* printf("dh scara\n"); */
      /* mprint(matrix4, 4, 4); */
      /* printf("\n"); */
      /* printf("\t  class=%d\n", fpclassify(matrix3[1])); */
      
      /* mprint(matrix2, 4, 4);       */
 /*      Delay(5000000); */
 /* printf("timer:%d\n", *DWT_CYCCNT); */
  while (1)
    {
      ;
    }

}

/**
  * @brief  Delay Function.
  * @param  nCount:specifies the Delay time length.
  * @retval None
  */
void Delay(__IO uint32_t nCount)
{
  while(nCount--)
  {
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
