#include <stdio.h>

void print(const float const *matrix, int h, int w)
{
    for(int i = 0; i<h; i++)
      {
      printf("| ");
	for(int j = 0; j<w; j++)
	  {
	    printf("%.9f ", 10, *(matrix + i*w + j));
	  }
      printf("|\n");
      }
}
