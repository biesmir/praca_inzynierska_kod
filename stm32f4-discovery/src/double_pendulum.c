#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void dh_edda(float q1, float q2, float l1, float l2, float* K)
{
	*(K + 0) = cosf(q1+q2);
	*(K + 1) = -sinf(q1+q2);
	*(K + 2) = 0.0;
	*(K + 3) = l1*cosf(q1)+l2*cosf(q1+q2);

	*(K + 4) = sinf(q1+q2);
	*(K + 5) = cosf(q1+q2);
	*(K + 6) = 0.0;
	*(K + 7) = l1*sinf(q1) + l2*sinf(q1+q2);

	*(K + 8) = 0.0;
	*(K + 9) = 0.0;
	*(K + 10) = 1.0;
	*(K + 11) = 0.0;

	*(K + 12) = 0.0;
	*(K + 13) = 0.0;
	*(K + 14) = 0.0;
	*(K + 15) = 1.0;

}

void dh_edda_double(double q1, double q2, double l1, double l2, double* K)
{
	*(K + 0) = cos(q1+q2);
	*(K + 1) = -sin(q1+q2);
	*(K + 2) = 0.0;
	*(K + 3) = l1*cos(q1)+l2*cos(q1+q2);

	*(K + 4) = sin(q1+q2);
	*(K + 5) = cos(q1+q2);
	*(K + 6) = 0.0;
	*(K + 7) = l1*sin(q1) + l2*sin(q1+q2);

	*(K + 8) = 0.0;
	*(K + 9) = 0.0;
	*(K + 10) = 1.0;
	*(K + 11) = 0.0;

	*(K + 12) = 0.0;
	*(K + 13) = 0.0;
	*(K + 14) = 0.0;
	*(K + 15) = 1.0;

}

void exp_edda_basic(const float q1, const float q2, const float l1, const float l2, float *K)
{
  printf("l1:%f\n", l1);
  printf("l2:%f\n", l2);  
  printf("l1+l2:%f\n", l1+l2);
  const float K0[16] = {1.0, 0.0, 0.0, l1+l2,
		  0.0, 1.0, 0.0, 0.0,
		  0.0, 0.0, 1.0, 0.0,
		  0.0, 0.0, 0.0, 1.0};
  printf("K0\n");
    print(K0,4,4);
  const float eps1[16] = {0, -q1, 0, 0,
		    q1, 0, 0, 0,
		    0, 0, 0, 0,
		    0, 0, 0, 0};
  
  const float eps2[16] = {0, -q2, 0, 0,
		    q2, 0, 0, -l1*q2,
		    0, 0, 0, 0,
		    0, 0, 0, 0}; 

  float tmp[16];
  float exp1[16];
  float exp2[16];
  matrix_exp_basic(eps1, exp1);
  matrix_exp_basic(eps2, exp2);
  printf("exp1\n");
    print(exp1,4,4);
  printf("exp2\n");
    print(exp2,4,4);
  printf("K0\n");
    print(K0,4,4);    
  matrix4x4_mul(exp2, K0, tmp);
  matrix4x4_mul(exp1, tmp, K);
  
}
