import numpy as np
import statistics as st
import pprint
import sys
from mpmath import mp

mp.dsp = 100000


def dh_edda(q1, q2, l1, l2):
    K = []
    K.append(mp.cos(q1+q2))
    K.append(-mp.sin(q1+q2))
    K.append(0.0)
    K.append(l1*mp.cos(q1)+l2*mp.cos(q1+q2))
    
    K.append(mp.sin(q1+q2))
    K.append(mp.cos(q1+q2))
    K.append(0.0)
    K.append(l1*mp.sin(q1) + l2*mp.sin(q1+q2))

    K.append(0.0)
    K.append(0.0)
    K.append(1.0)
    K.append(0.0)
    
    K.append(0.0)
    K.append(0.0)
    K.append(0.0)
    K.append(1.0)
    return np.array(K)



if __name__=='__main__':
    Kpy = []
    errors = []
    pp = pprint.PrettyPrinter(indent=4)
    with open(sys.argv[1], 'r') as log:
        log.readline()
        while True:
            try:
                Kpy = []
                i = log.readline()
                #if i[0]=='i':
                 #   break
                q1 = float(log.readline())
                q2 = float(log.readline())
                l1 = float(log.readline())
                l2 = float(log.readline())        
                for i in range(4):
                    line = log.readline()
                    line = line.split(' ')
                    for i in range(1, 5):
                        Kpy.append(float(line[i]))
                K = dh_edda(q1, q2, l1, l2)
                for i in range(len(Kpy)):
                    try:
                        err = (K[i]-Kpy[i])/Kpy[i]
                        errors.append(abs(err))
                    except ZeroDivisionError:
                        pass
            except ValueError:
                break
    errors = list(filter((0.0).__ne__, errors))
    errors = [x for x in errors if str(x) != 'nan']
    print("min")
    print(min(errors))
    print("max")
    print(max(errors))
    print("stdev")
    print(st.stdev(errors))
    # # print(errors)
    print('avg')
    print(np.mean(errors))


#    pp.pprint(errors)
